# See http://docs.opscode.com/config_rb_knife.html for more information on knife configuration options

current_dir = File.dirname(__FILE__)
log_level                :info
log_location             STDOUT
node_name                "damien_stanton"
client_key               "#{current_dir}/damien_stanton.pem"
validation_client_name   "dks-validator"
validation_key           "#{current_dir}/dks-validator.pem"
chef_server_url          "https://api.opscode.com/organizations/dks"
cache_type               'BasicFile'
cache_options( :path => "#{ENV['HOME']}/.chef/checksums" )
cookbook_path            ["#{current_dir}/../cookbooks"]
