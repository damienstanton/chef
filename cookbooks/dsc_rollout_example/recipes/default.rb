#
# Cookbook Name:: dsc_rollout_example
# Recipe:: default
# Author:: Damien Stanton
# Copyright (C) 2014 
#
# 
#

depends "dsc"

dsc_resource "MSFT_xADDomain" do
	resource_name :xADdomain
	property :domainName, "contosso.com"
	property :parentDomainName, "redmond.com"
	property :domainAdminCred, "secret"
	property :safemodeAdminCred, "secret"
	property :dnsDelegationCred, "secret"
end

dsc_resource "MSFT_xADDomainController" do
	resource_name :xADdomaincontroller
	property :domainName, "contosso.com"
	property :domainAdminCred, "secret"
	property :safemodeAdminCred, "secret"
end

dsc_resource "MSFT_xADUser" do
	resource_name :xaduser
	property :domainName, "contosso.com"
	property :username, "DStanton"
	property :password, "secret"
	property :domainAdminCred, "secret"
end

dsc_resource "MSFT_xIPAddress" do
	resource_name :xipaddress
	property :ipaddress, "192.168.1.50"
	property :interfacealias, "Ethernet"
	property :defaultgateway, "192.168.1.1"
	property :subnetmask, 255.255.255.0
	property :addressfamily, "IPv4"
end

dsc_resource "MSFT_xWindowsOptionalFeature" do
	resource_name :xwindowsoptionalfeature
	property :name, "TelnetClient"
	property :ensure, "Present"
	property :source, "X:\installmedia"
	property :nowindowsupdatecheck, true
	property :removefilesondisable, false
	property :loglevel, "ErrorsAndWarningAndInformation"
	property :logpath, "#{ENV['USERPROFILE']}/Logs"
end
