name             'dsc_rollout_example'
maintainer       'Damien Stanton'
maintainer_email 'damien.stanton@gmail.com'
license          'Proprietary - All Rights Reserved'
description      'Installs/Configures dsc_rollout_example'
long_description 'Installs/Configures dsc_rollout_example'
version          '0.1.0'

